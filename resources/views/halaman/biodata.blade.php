@extends('layout.master')

@section('judul')
Halaman Biodata
@endsection

@section('content')
    <form action="/kirim" method="post">
        @csrf
        <label>Nama Lengkap</label> <br> <br>
        <input type="text" name="nama"> <br> <br>
        <label>Biodata</label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>

        <label> Jenis Kelamin </label> <br> <br>
        <input type="radio" name ="jk" value = 1 >Laki Laki <br> 
        <input type="radio" name ="jk" value = 2 >Perempuan <br> <br>

        <input type="submit" value='kirim'>
    </form>    
@endsection