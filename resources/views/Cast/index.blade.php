@extends('layout.master')

@section('judul')
Halaman List Casting
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success mb-1">Add Data</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
<tbody>
        @forelse ($cast as $key=> $item)
        <tr>
            <td>{{ $key+ 1 }}</td>
            <td>{{ $item->nama}}</td>
            <td>{{ $item->umur}}</td>
            <td>{{ $item->bio}}</td>    
            <td>
                <form action="/cast/{{$item->id}}" methode="POST">
                    <a href="/cast/{{$item->id}}"      class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
            
        @empty
        <tr>
            <td>No data, still empty</td>      
        </tr>
            
        @endforelse
    </tbody>
  </table>
  @endsection