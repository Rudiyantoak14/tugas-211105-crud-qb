@extends('layout.master')

@section('judul')
Halaman Edit Casting  {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{ $cast->id}} " method="POST">
     @csrf 
     @method('PUT')
    <div class="form-group">
      <label>Nama Casting </label>  
      <input type="text" name="nama" value= {{$cast->nama}} class="form-control" >
    </div>
     @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
     @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="integer" class="form-control" name="umur" value= {{$cast->umur}}  id="" placeholder="Masukkan Umur">
    </div>
     @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
     @enderror

    <div class="form-group form-check">
        <label>Biodata </label>
        <textarea name="bio" id="" cols="30" rows="10" >{{$cast->bio}} </textarea>
        <p></p> 
    </div>
     @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
     @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection