<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test', function () {
//     return view('welcome');
// });

// Route::get('/','HomeController@index' );
// Route::get('/form','formController@form');
// Route::post('/kirim','formController@kirim');

Route::get('/','IndexController@home');
Route::get('/register','AuthController@register');
Route::post('/welcome','AuthController@kirim');

// Route::get('/master', function() {
//     return view('layout.master');
// });

Route::get('/table', function() {
    return view('table.table');
});

Route::get('/data-table', function() {
    return view('table.data-table');
});

// Route::get('/cast', function() {
//     return view('cast.index');
// });
// CRUD CAST

Route::get('/cast/create','Castcontroller@create');
Route::post('/cast'      ,'Castcontroller@store');
Route::get('/cast'       ,'Castcontroller@index');

Route::get('/cast/{cast_id}','Castcontroller@show');
Route::get('/cast/{cast_id}/edit','Castcontroller@edit');

Route::put('/cast/{cast_id}','Castcontroller@update');
Route::delete('/cast/{cast_id}','Castcontroller@destroy');

