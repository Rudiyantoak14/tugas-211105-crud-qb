<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form(){
        return view('halaman.biodata');
    }

    public function kirim(Request $request){
        // dd($request->all());
        $nama    = $request ->nama;
        $biodata = $request ->bio;
        $jk      = $request ->jk;

        return view('halaman.home', compact('nama','biodata','jk'));

    }
}
